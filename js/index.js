  $(function () {
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
      $('.carousel').carousel({interval: 4000})
  });

      $('#contacto').on('show.bs.modal', function(e){ 
        console.log('El modal se está mostrando');

        $('#contactoBtn').removeClass('btn-outline-primary');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);

      });

       $("#contacto").on("shown.bs.modal", function (e) {
      console.log("Abierto modal");
        });

        $("#contacto").on("hide.bs.modal", function (e) {
        console.log("el modal se oculta");
        });

      $('#contacto').on('hidden.bs.modal', function(e){ 
        console.log('El modal se ocultó');
        $('#contactoBtn').prop('disabled', false);

      });
